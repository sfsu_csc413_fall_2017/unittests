package unitTests;

import java.util.HashMap;

public class UrlParser {

  private String url;
  private String endpoint;
  private HashMap<String,String> args;

  public UrlParser(String url) {
    this.url = url;
    args = new HashMap<>();
    this.parse();
  }

  private void parse(){
    // First split the parts
    String[] parts = url.split("\\?");
    if(parts.length == 1){
      // if nothing after ?, then no params
      this.endpoint = this.url;
    }else{
      // if so, then endpoint is part 0
      this.endpoint = parts[0];
      // split and see if there are multiple args
      String[] argParts = parts[1].split("&");
      for (int i = 0; i < argParts.length; i ++){
        // Add args to a map
        String queryParts[] = argParts[i].split("=");
        args.put(queryParts[0], queryParts[1]);
      }
    }
  }

  public String getEndpoint() {
    return this.endpoint;
  }

  public String getParam(String key){
    return this.args.get(key);
  }

}
