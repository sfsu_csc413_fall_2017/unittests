package unitTests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class UrlParserTest {

  @Test
  void getParam() {
    UrlParser parser = new UrlParser("/users?somaer=asd");
    assertEquals(parser.getParam("somaer"), "asd", "First Param");

    parser = new UrlParser("/users?somaer=asd&key=value");
    assertEquals(parser.getParam("somaer"), "asd", "multiple args first");
    assertEquals(parser.getParam("key"), "value", "multiple args second");
    assertEquals(parser.getParam("wrong"), null, "param that doesn't exist");
  }

  @Test
  void getEndpoint() {
    UrlParser parser = new UrlParser("/users?somaer=asd");
    assertEquals(parser.getEndpoint(), "/users", "Parser with random parameter");

    parser = new UrlParser("/users");
    assertEquals(parser.getEndpoint(), "/users", "No args");

    parser = new UrlParser("/");
    assertEquals(parser.getEndpoint(), "/", "Root endpoint");

    parser = new UrlParser("/users?somaer=asd&key=value");
    assertEquals(parser.getEndpoint(), "/users", "multiple args");
  }

}